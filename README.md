# Tiapp Push Notifications - Drupal Module

This Tiapp Drupal module allows administrators to send push-notifications to the mobile app via Appecelerator's REST API. It supports direct messages to mobile users and data update notifications.


#### Dependencies

- HTTPFul ( http://phphttpclient.com )
- Drupal Modules: 
  - entity
  - stanford_date_timepicker
  - maxlength


#### Other Requirements

- See Appcelerator's push notifications guide for details for creating a development account and an iOS certificate.


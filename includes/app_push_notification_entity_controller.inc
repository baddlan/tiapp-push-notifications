<?php
/**
 * Custom controller for the notification entity.
 */
class TiappPushNotificationEntityController extends EntityAPIController {

  function __construct() {
    parent::__construct(TIAPP_PUSH_NOTIFICATION_ENTITY_TYPE);
  }

  public function query($ids, $conditions, $revision_id = FALSE) {
    // Build the query.
    $query = $this->buildQuery($ids, $conditions, $revision_id);
    $result = $query->execute();
    return $result;
  }

  /**
   * Override the save method.
   */
  public function save($entity, DatabaseTransaction $transaction = NULL) {
    global $user;
    $entity->changed = REQUEST_TIME;
    $entity->is_new = !empty($entity->is_new) || empty($entity->{$this->idKey});

    // Set the created date and assign the author only the first time the
    // entity is saved
    if ($entity->is_new) {
      $entity->created = REQUEST_TIME;
      $entity->uid     = $user->uid;
    }

    // Reset the notification status to `pending` if it has already been pushed
    // and received an `error` status in order to have it re-scheduled
    if (isset($entity->status) && $entity->status == TIAPP_PUSH_STATUS_ERROR) {
      $entity->status = TIAPP_PUSH_STATUS_PENDING;
    }

    return parent::save($entity, $transaction);
  }
}

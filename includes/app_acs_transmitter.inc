<?php
/**
 * @dependencies HTTPFul ( http://phphttpclient.com )
 */

module_load_include('phar', 'tiapp_push_notification', 'lib/httpful');

define('ACS_SESSION_DURATION', 2 * 60);

class TiappACSTransmitter {

  private static $instance  = null;
  private $configSource     = 'drupal';

  /**
   * Default settings
   */
  private $settings = array(
    'apiUrlPrefix'    => 'https://api.cloud.appcelerator.com/v1',
    'apiKey'          => 'GET THE API KEY FROM ACS PLATFORM AND INJECT IT INTO DRUPAL VARIABLE "tiapp_acs_settings" OR INSERT IT HERE, SEND A PUSH NOTIFICATION TO STORE IT AND THEN RESET THIS LINE',
    'username'        => 'YOUR_ACS_USERNAME', // default user (don't forget to create it in ACS platform)
    'password'        => 'YOUR_ACS_PASSWORD', // default password (don't forget to create it in ACS platform)
    'sessionDuration' => ACS_SESSION_DURATION // in seconds (default: 2 minutes)
  );

  private $curlOpts = array();


  private function __construct() {
    $this->ensureCredentials();
  }

  public static function getInstance() {
    if (self::$instance === null)
      self::$instance = new TiappACSTransmitter();

    return self::$instance;
  }

  // // // // // // // // // // // // // // // // // // // // // // // // // //
  // Getters and Setters
  // // // // // // // // // // // // // // // // // // // // // // // // // //

  /**
   * @return string
   */
  public function getConfigSource() {
    return $this->configSource;
  }

  /**
   * @param string $configSource
   */
  public function setConfigSource($configSource) {
    $this->configSource = $configSource;
  }

  /**
   * @return null
   */
  public function getSettings() {
    return $this->settings;
  }

  /**
   * @param null $settings
   */
  public function setSettings($settings) {
    $this->settings = $settings;
  }


  // // // // // // // // // // // // // // // // // // // // // // // // // //
  // Utility methods
  // // // // // // // // // // // // // // // // // // // // // // // // // //

  /**
   * Load the settings, including credentials, from the `configSource`, if it
   * has not already been already loaded. Then validate whether the token,
   * username and password are set.
   *
   * @return bool
   */
  public function ensureCredentials() {
    if ($this->configSource == 'drupal') {
      $drupal_settings = json_decode(variable_get('tiapp_acs_settings', '{}'), true);
      $this->settings  = array_merge($this->settings, $drupal_settings);
    }

    return !empty($this->settings['apiKey'])
        && !empty($this->settings['username'])
        && !empty($this->settings['password']);
  }

  protected function initRequest() {
    $this->curlOpts = array(
      CURLOPT_URL             => 'https://api.cloud.appcelerator.com/v1/users/login.json?key='.$this->settings['apiKey'],
      CURLOPT_POSTFIELDS      => "login=".$this->settings['username']."&password=".$this->settings['password'],
      CURLOPT_COOKIEJAR       => $this->settings['cookieFile'],
      CURLOPT_COOKIEFILE      => $this->settings['cookieFile'],
      CURLOPT_RETURNTRANSFER  => true,
      CURLOPT_POST            => 1,
      CURLOPT_FOLLOWLOCATION  => 1,
      CURLOPT_TIMEOUT         => 60
    );
  }

  protected function saveSettings() {
    variable_set('tiapp_acs_settings', json_encode($this->settings));
  }

  protected function urlForCommand($command) {
    return $this->settings['apiUrlPrefix'].$command.'?key='.$this->settings['apiKey'];
  }

  // // // // // // // // // // // // // // // // // // // // // // // // // //
  // Notification Messages
  // // // // // // // // // // // // // // // // // // // // // // // // // //

  /**
   *
   * Upon a successful login, the session_id received from ACS will be stored
   * in the settings along with a future expiry time based on the
   * @return mixed
   * @throws \Exception
   */
  protected function login() {
    // First, check whether a valid session exists and has not expired
    if (  isset($this->settings['sessionId'])
      && !empty($this->settings['sessionId'])
      &&  isset($this->settings['sessionExpiry'])
      &&  is_numeric($this->settings['sessionExpiry'])
      && $this->settings['sessionExpiry'] >= time())
    {
      return $this->settings['sessionId'];
    }

    $url      = $this->urlForCommand('/users/login.json');
    $payload  = array(
      'login'    => $this->settings['username'],
      'password' => $this->settings['password'],
    );

    $response = \Httpful\Request::post($url, $payload)
      ->sendsJson()
      ->send();

    if ($response->body->meta->status === 'ok') {
      $this->settings['sessionId']     = $response->body->meta->session_id;
      $this->settings['sessionExpiry'] = time() + $this->settings['sessionDuration'];
      $this->saveSettings();
      return $this->settings['sessionId'];
    }
    else {
      throw new TiappACSTransmitterException('Invalid ACS Session ID. This error indicates a missing Session ID and a failure to obtain one through a login request. '.json_encode($response->body->meta));
    }
  }

  /**
   * An abstraction for sending push notifications of various types by invoking
   * the ACS `/push_notification/notify.json` command.
   *
   * @param array $payload The notification payload
   * @param string $channel The name of the subscription channel to which the
   *        message is destined. This should be `null` if message targets all
   *        app users and/or limited by IDs from the $to parameter.
   * @param string|array $to Comma-separated list of destination subsribers' IDs
   *        or the string `everyone` for all users in channel (or entire app).
   * @throws \Exception Throws a generic exception if the response returned
   *         anything other than an `ok` status.
   * @see http://docs.appcelerator.com/arrowdb/latest/#!/api/PushPayload
   * @return stdClass|null The notification response object or null otherwise.
   */
  public function push(array $payload, $channel = 'default', $to = 'everyone') {
    $sessionId = $this->login();
    $url       = $this->urlForCommand('/push_notification/notify.json');
    $data      = array(
      'payload'  => json_encode($payload)
    );

    if (is_array($to))
      $data['to_ids'] = implode(',', $to);
    else if (is_string($to))
      $data['to_ids'] = $to;
    else
      throw new Exception('Unsupported push notification target. The $to argument of push is invalid.');

    if (!empty($channel))
      $data['channel'] = $channel;

    $response = \Httpful\Request::post($url, $data)
      ->sendsJson()
      ->addHeader('session_id', $sessionId)
      ->addHeader('Cookie', '_session_id='.$sessionId)
      ->send();

    if ($response->body->meta->status === 'ok' && isset($response->body->response->push_notification)) {
      return $response->body;
    }
    else {
      throw new TiappACSTransmitterException('Push notification failed. '.json_encode($response->body));
    }
  }

  /**
   * @param $notification stdClass A `tiapp_push_notification` entity instance
   * @throws \Exception @see TiappACSTransmitter::push()
   * @return stdClass|null The notification response object or null otherwise.
   */
  public function pushAlert($notification) {
    $payload = $this->getAlertPayloadFromNotification($notification);
    return $this->push($payload);
  }

  public function pushAlertTest($notification) {
    $payload   = $this->getAlertPayloadFromNotification($notification);
    $succeeded = (rand() % 2) == 0;

    $response = array(
      'body' => array(
        'meta' => array(
          'code' => $succeeded ? 200 : 404,
          'status' => $succeeded ? 'ok' : 'failed',
          'method_name' => 'Notify'
        )
      ),
      'push_notification' => array(
        'id' => substr(md5(uniqid(rand(), true)), 0, 20),
        'payload' => json_encode($payload),
        'channel' => 'default'
      )
    );

    // recursively convert response array to object
    return json_decode(json_encode($response));
  }

  protected function getAlertPayloadFromNotification($notification) {
    $payload  = array(
      'alert'          => $notification->message,
//      'category'       => 'sampleCategory',
      'icon'           => 'notification',
      'sound'          => 'default',
      'title'          => $notification->title,
      'vibrate'        => true,
      'command'        => 'popup',
      'link'           => $notification->link,
      'params'         => '{}'
    );

    return $payload;
  }

  /**
   * Todo: To be implemented when requirements are further clarified
   * Send a push notification with an attached reference to a Resource. Tapping
   * the notification alert on the device would potentially trigger the App to
   * launch (if not already on the foreground) and navigate to the Resource.
   * @throws \Exception
   */
  public function pushAlertWithResourceLink() {
    throw new Exception('Not Implemented!');
  }

  /**
   * Send a push notification to the mobile app with a badge increment to
   * indicate the availability of new updates. This function would typically be
   * invoked at the end of a successful export process in CMS.
   * @param $notification stdClass A `tiapp_push_notification` entity instance
   * @throws \Exception
   * @return stdClass|null The notification response object or null otherwise.
   */
  public function pushUpdate($notification) {
    $payload  = array(
      'alert'          => $notification->message,
      'badge'          => '+1',
//      'sound'          => '',
      'title'          => $notification->title,
      'vibrate'        => false,
      'command'        => 'update',
      'params'         => '{}',
//      'notes'          => 'Update Notification with a title and an alert message (for background activity)'
//      'category'       => 'contentUpdate',
      'icon'           => 'notification',
    );

    return $this->push($payload);
  }

  public function test() {
//    $url = $this->settings['apiUrlPrefix'].'/push_notification/count.json?key='.$this->settings['apiKey'];
//    $response = \Httpful\Request::get($url)->send();
//    var_dump($response);
    $this->login();

    $notification = tiapp_push_notification_load(1);
    if ($notification) {
      $this->pushAlert($notification);
    }
    else {
      throw new Exception('TiappACSTrasmitter::test: Unable to find Notification with ID (1)');
    }
  }
}

class TiappACSRequest {


  function __construct() {
  }
}

class TiappACSTransmitterException extends Exception {}
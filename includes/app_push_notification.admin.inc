<?php
/**
 * @file
 * The file for admin forms and functionality for the notifications entity
 */

/*
 * Example JSON response:
  {
    "meta": {
      "code": 200,
      "status": "ok",
      "method_name": "Notify"
    },
    "response": {
      "push_notification": {
        "id": "53690d3fe10fa4582a00e887",
        "payload": "test",
        "channel": "friend_request"
      }
    }
  }
*/

/**
 * Implements hook_form().
 */
function tiapp_push_notification_form($form, &$form_state, $notification = NULL) {
  $isSent = isset($notification)
                           && isset($notification->status)
                           && $notification->status == TIAPP_PUSH_STATUS_SENT;
  $form = array();

  $form['notification_id'] = array(
    '#title' => t('Notification ID'),
    '#type' => 'hidden',
    '#value' => isset($notification->notification_id) ? $notification->notification_id : '',
  );

  $form['response_id'] = array(
    '#title' => t('Response ID'),
    '#type' => 'hidden',
    '#value' => isset($notification->notification_id) ? $notification->notification_id : '',
  );
//

  $form['type'] = array(
    '#title' => t('Type'),
    '#type' => 'select',
    '#default_value' => isset($notification->type) ? tiapp_push_notification_type($notification->type) : tiapp_push_notification_type(TIAPP_PUSH_TYPE_ALERT),
    '#description' => t('Select "Alert" for simple text notifications or "Update" to indicate content update.'),
    '#options' => array(
      TIAPP_PUSH_TYPE_ALERT  => tiapp_push_notification_type(TIAPP_PUSH_TYPE_ALERT),
      TIAPP_PUSH_TYPE_UPDATE => tiapp_push_notification_type(TIAPP_PUSH_TYPE_UPDATE),
    ),
    '#required' => TRUE,
    '#disabled' => $isSent,
  );

  $form['push_date'] = array(
    '#title' => t('Scheduled push date'),
    '#type' => 'date_popup',
    '#default_value' => isset($notification->push_date) ? $notification->push_date : '',
    '#description' => t('The time and date on which the notification is scheduled for submission.'),
    '#required' => TRUE,
    '#disabled' => $isSent,
    '#date_format' => 'm/d/Y - H:i',
    '#date_increment' => '15',
    '#date_year_range' => '-1:+1',
    '#date_label_position' => 'above',
  );

  $form['title'] = array(
    '#title' => t('Title'),
    '#type' => 'textfield',
    '#default_value' => isset($notification->title) ? $notification->title : '',
    '#description' => t('The title of the notification as it appears in the mobile device. This is also used in notifications list.'),
    '#required' => FALSE,
    '#disabled' => $isSent,
    '#size' => 120,
    '#length' => 128,
    '#maxlength' => 128,
    '#maxlength_js' => TRUE,
  );

  $form['message'] = array(
    '#title' => t('Message'),
    '#type' => 'textarea',
    '#default_value' => isset($notification->message) ? $notification->message : '',
    '#description' => t('The body text of the notification.'),
    '#required' => FALSE,
    '#disabled' => $isSent,
    '#length' => 255,
    '#maxlength' => 255,
    '#maxlength_js' => TRUE,
    '#rows' => '3',
  );

  $form['link'] = array(
    '#title' => t('Link'),
    '#type' => 'textfield',
    '#default_value' => isset($notification->link) ? $notification->link : '',
    '#description' => t('An optional link (URL) to a web resource.'),
    '#required' => FALSE,
    '#disabled' => $isSent,
    '#size' => 120,
    '#length' => 128,
    '#maxlength' => 128,
    '#maxlength_js' => TRUE,
  );

  field_attach_form('notifications', $notification, $form, $form_state);

  $form['actions'] = array(
    '#type' => 'actions',
    'submit' => array(
      '#type' => $isSent ? '' : 'submit',
      '#value' => isset($notification->notification_id) ? t('Update') : t('Save'),
      '#disabled' => $isSent,
    ),
    'cancel' => array(
      '#markup' => l(t('Cancel'), ADMIN_CONTENT_NOTIFICATIONS_URI),
    ),
  );

  return $form;
}

/**
 * Implements hook_form_validate().
 */
function tiapp_push_notification_form_validate($form, &$form_state) {
  ;
}

/**
 * Implements hook_form_submit().
 */
function tiapp_push_notification_form_submit($form, &$form_state) {
  $form_state['entity_type'] = TIAPP_PUSH_NOTIFICATION_ENTITY_TYPE;
  $notification = null;

  if (isset($form_state['values']['notification_id']) && !empty($form_state['values']['notification_id']))
    $notification = tiapp_push_notification_load($form_state['values']['notification_id']);
  else
    $notification = new stdClass();

  // Disallow saving notifications that have already been pushed successfully
  if (isset($notification->status) && ($notification->status == TIAPP_PUSH_STATUS_SENT)) {
    drupal_set_message(t('Attempt to save a notification that has already been pushed with title: "@title"', array('@title' => $notification->title)), 'warning');
  }
  else {
    $form_state[TIAPP_PUSH_NOTIFICATION_ENTITY_TYPE] = $notification;
    $controller = new TiappPushNotificationEntityController();
    $notification = entity_ui_form_submit_build_entity($form, $form_state);
    $controller->save($notification);

    drupal_set_message(t('"@title" notification has been saved.', array('@title' => $notification->title)));
  }

//  $form_state['redirect'] = ADMIN_CONTENT_NOTIFICATIONS_URI.'/'.$notification->notification_id;
  $form_state['redirect'] = ADMIN_CONTENT_NOTIFICATIONS_URI;
}

/**
 * Confirmation before bulk deleting notifications.
 */
function tiapp_push_notification_bulk_delete($form, &$form_state, $notifications_ids) {
  $notifications_ids = explode('|', $notifications_ids);
  $notifications = notifications_load_multiple($notifications_ids);

  $form = array();
  $form_state['notifications_ids'] = $notifications_ids;

  $variables = array(
    'type' => 'ul',
    'items' => array(),
    'title' => '',
    'attributes' => array(),
  );

  foreach ($notifications as $notification) {
    $variables['items'][] = $notification->title;
  }

  $form['summary'] = array(
    '#markup' => theme_item_list($variables),
  );

  return confirm_form($form, t('Delete all notifications?'), ADMIN_CONTENT_NOTIFICATIONS_URI, t('Placeholder description'), t('Delete all'), t('Cancel'));
}

/**
 * Implements hook_submit().
 */
function tiapp_push_notification_bulk_delete_submit($form, &$form_state) {
  $notifications_ids = $form_state['notifications_ids'];
  notifications_delete_multiple($notifications_ids);
  drupal_set_message(t('notifications deleted'));
  drupal_goto(ADMIN_CONTENT_NOTIFICATIONS_URI);
}

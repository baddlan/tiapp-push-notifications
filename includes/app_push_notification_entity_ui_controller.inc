<?php
/**
 * Custom controller for the administrator UI.
 */
class TiappPushNotificationEntityUIController extends EntityDefaultUIController {

  /**
   * Override the menu hook for default ui controller.
   */
  public function hook_menu() {
    $items = parent::hook_menu();
    $items[$this->path]['title'] = t('Push Notifications');
    $items[$this->path]['description'] = t('Manage notifications, including fields.');
    $items[$this->path]['access callback'] = 'tiapp_push_notification_access_callback';
    $items[$this->path]['access arguments'] = array('administer push notifications entities');
    $items[$this->path]['type'] = MENU_NORMAL_ITEM;
    return $items;
  }

  /**
   * Admin form for searching and doing bulk operations.
   */
  public function overviewForm($form, &$form_state) {
    $form['pager'] = array('#theme' => 'pager');

    $header = array(
      'notification_id' => array('data' => 'Notification ID', 'field' => 'notification_id'),
      'type' => array('data' => 'Type', 'field' => 'type'),
      'title' => array('data' => 'Title', 'field' => 'title'),
      'push_date' => array('data' => 'Push Date', 'field' => 'push_date'),
      'status' => array('data' => 'Status', 'field' => 'status'),
//      'message' => array('data' => t('Message'), 'field' => 'message'),
      'operations' => array('data' => 'Operations', 'field' => 'operations'),
    );

    $options = array();
    $search_term = !empty($_GET['search']) ? $_GET['search'] : NULL;

    $query = new EntityFieldQuery();
    $query->entityCondition('entity_type', TIAPP_PUSH_NOTIFICATION_ENTITY_TYPE);

    if (!empty($search_term)) {
      $query->propertyCondition('message', '%' . $search_term . '%', 'like');
    }

    // Check for sort order and sort key.
    if (!empty($_GET['sort']) && !empty($_GET['order'])) {
      $sort = strtoupper($_GET['sort']);
      $order = strtolower($_GET['order']);
      $order = str_replace(' ', '_', $order);
      if ($order != 'operations') {
        $query->propertyOrderBy($order, $sort);
      }
    }
    else {
        $query->propertyOrderBy('push_date', 'DESC');
    }

    $query->pager(TOTAL_ITEMS_PER_PAGE);

    $result = $query->execute();
    $notifications_results = !empty($result[TIAPP_PUSH_NOTIFICATION_ENTITY_TYPE]) ? $result[TIAPP_PUSH_NOTIFICATION_ENTITY_TYPE] : array();
    $notifications_array = !empty($notifications_results) ? tiapp_push_notification_load_multiple(array_keys($notifications_results)) : array();

    foreach ($notifications_array as $notification_id => $notification) {
      $isSent = isset($notification->status) && $notification->status == TIAPP_PUSH_STATUS_SENT;

      $options['notification-id-' . $notification_id] = array(
        'notification_id' => $notification->notification_id,
        'type'            => tiapp_push_notification_type($notification->type),
        'title'           => strlen($notification->title) > 85 ? substr($notification->title, 0, 85)."..." : $notification->title,
//        'message'         => strlen($notification->message) > 50 ? substr($notification->message, 0, 50)."..." : $notification->message,
        'push_date'       => $notification->push_date,
        'status'          => tiapp_push_notification_status_text($notification->status),
        'operations'      =>
          l(($isSent ? t('View') : t('Edit')), ADMIN_CONTENT_NOTIFICATIONS_MANAGE_URI . '/' . $notification_id, array('query' => array('destination' => ADMIN_CONTENT_NOTIFICATIONS_URI))) . ' &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;' .
          l(t('Delete'), ADMIN_CONTENT_NOTIFICATIONS_MANAGE_URI . '/' . $notification_id . '/delete', array('attributes' => array('class' => array('notifications-delete-' . $notification->notification_id), ), 'query' => array('destination' => ADMIN_CONTENT_NOTIFICATIONS_MANAGE_URI))),
      );
    }

    $form['entities'] = array(
      '#type' => 'tableselect',
      '#header' => $header,
      '#options' => $options,
      '#attributes' => array('class' => array('entity-sort-table')),
      '#empty' => t('There are no notifications.'),
    );

    return $form;
  }

  /**
   * Form Submit method.
   */
  public function overviewFormSubmit($form, &$form_state) {
    $values = $form_state['input'];
    $notifications_ids = array();

    if (!empty($values['entities'])) {
      foreach ($values['entities'] as $index => $value) {
        if (!empty($value)) {
          $notifications_ids[] = str_replace('notification-id-', '', $value);
        }
      }

      switch ($values['operations']) {
        case 'delete':
          drupal_goto(ADMIN_CONTENT_NOTIFICATIONS_URI.'/bulk/delete/' . implode('|', $notifications_ids));
          break;
      }
    }

    if (!empty($values['search_text'])) {
      drupal_goto(ADMIN_CONTENT_NOTIFICATIONS_URI, array('query' => array('search' => $values['search_text'])));
    }
  }
}

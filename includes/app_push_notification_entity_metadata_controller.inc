<?php
/**
 * Custom controller for the notification entity.
 */
class TiappPushNotificationEntityMetadataController extends EntityDefaultMetadataController {

  public function entityPropertyInfo() {
    $info = parent::entityPropertyInfo();
    $properties = &$info[TIAPP_PUSH_NOTIFICATION_ENTITY_TYPE]['properties'];

    $properties['status'] = array(
      'label' => t("Status"),
      'description' => t("Whether the node is published or unpublished."),
      // Although the status is expected to be boolean, its schema suggests
      // it is an integer, so we follow the schema definition.
      'type' => 'integer',
      'options list' => 'entity_metadata_status_options_list',
      'setter callback' => 'entity_property_verbatim_set',
      'setter permission' => 'administer push notifications',
      'schema field' => 'status',
    );

    $properties['created'] = array(
      'label' => t("Date created"),
      'type' => 'date',
      'description' => t("The date the node was posted."),
      'setter callback' => 'entity_property_verbatim_set',
      'setter permission' => 'administer push notifications',
      'schema field' => 'created',
    );

    $properties['changed'] = array(
      'label' => t("Date changed"),
      'type' => 'date',
      'description' => t("The date the node was most recently updated."),
      'schema field' => 'changed',
    );

    $properties['uid'] = array(
      'label' => t("Author"),
      'type' => 'user',
      'description' => t("The author of the notification."),
      'setter callback' => 'entity_property_verbatim_set',
      'setter permission' => 'administer push notifications',
      'required' => TRUE,
      'schema field' => 'uid',
    );

    return $info;
  }
}

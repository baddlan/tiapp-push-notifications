<?php
/**
 * Tiapp Push Notification custom entity class.
 */
class TiappPushNotification extends Entity {
  /**
   * Override defaultUri().
   */
  protected function defaultUri() {
    return array('path' => ADMIN_CONTENT_NOTIFICATIONS_URI.'/'. $this->identifier());
  }
}
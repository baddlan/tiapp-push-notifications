# $Id$
#
# LANGUAGE translation of Drupal (general)
# Copyright YEAR NAME <EMAIL@ADDRESS>
# Generated from files:
#  sites/all/modules/custom/tiapp/modules/tiapp-push-notification/tiapp_push_notification.module: n/a
#  sites/all/modules/custom/tiapp/modules/tiapp-push-notification/includes/tiapp_push_notification.admin.inc: n/a
#  sites/all/modules/custom/tiapp/modules/tiapp-push-notification/includes/tiapp_push_notification.theme.inc: n/a
#  sites/all/modules/custom/tiapp/modules/tiapp-push-notification/tiapp_push_notification.info: n/a
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PROJECT VERSION\n"
"POT-Creation-Date: 2015-10-01 10:15-0400\n"
"PO-Revision-Date: YYYY-mm-DD HH:MM+ZZZZ\n"
"Last-Translator: NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <EMAIL@ADDRESS>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=utf-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=INTEGER; plural=EXPRESSION;\n"

#: sites/all/modules/custom/tiapp/modules/tiapp-push-notification/tiapp_push_notification.module:23;360;65
msgid "Push Notifications"
msgstr ""

#: sites/all/modules/custom/tiapp/modules/tiapp-push-notification/tiapp_push_notification.module:164
msgid "Administer push notifications Entities"
msgstr ""

#: sites/all/modules/custom/tiapp/modules/tiapp-push-notification/tiapp_push_notification.module:165
msgid "Allows a user to administer push notification entities"
msgstr ""

#: sites/all/modules/custom/tiapp/modules/tiapp-push-notification/tiapp_push_notification.module:168
msgid "View Tiapp Push Notifications Entity"
msgstr ""

#: sites/all/modules/custom/tiapp/modules/tiapp-push-notification/tiapp_push_notification.module:169
msgid "Allows a user to view the push notification entities."
msgstr ""

#: sites/all/modules/custom/tiapp/modules/tiapp-push-notification/tiapp_push_notification.module:172
msgid "Create Tiapp Push Notifications Entities"
msgstr ""

#: sites/all/modules/custom/tiapp/modules/tiapp-push-notification/tiapp_push_notification.module:173
msgid "Allows a user to create push notification entities."
msgstr ""

#: sites/all/modules/custom/tiapp/modules/tiapp-push-notification/tiapp_push_notification.module:176
msgid "Edit Tiapp Push Notifications Entities"
msgstr ""

#: sites/all/modules/custom/tiapp/modules/tiapp-push-notification/tiapp_push_notification.module:177
msgid "Allows a user to edit push notification entities."
msgstr ""

#: sites/all/modules/custom/tiapp/modules/tiapp-push-notification/tiapp_push_notification.module:180
msgid "Delete Tiapp Push Notifications Entities"
msgstr ""

#: sites/all/modules/custom/tiapp/modules/tiapp-push-notification/tiapp_push_notification.module:181
msgid "Allows a user to delete push notification entities."
msgstr ""

#: sites/all/modules/custom/tiapp/modules/tiapp-push-notification/tiapp_push_notification.module:184
msgid "Do bulk operations on Tiapp Push Notifications entities"
msgstr ""

#: sites/all/modules/custom/tiapp/modules/tiapp-push-notification/tiapp_push_notification.module:185
msgid "Allows a user to do bulk operations."
msgstr ""

#: sites/all/modules/custom/tiapp/modules/tiapp-push-notification/tiapp_push_notification.module:361
msgid "Manage notifications, including fields."
msgstr ""

#: sites/all/modules/custom/tiapp/modules/tiapp-push-notification/tiapp_push_notification.module:375 sites/all/modules/custom/tiapp/modules/tiapp-push-notification/includes/tiapp_push_notification.admin.inc:33
msgid "Notification ID"
msgstr ""

#: sites/all/modules/custom/tiapp/modules/tiapp-push-notification/tiapp_push_notification.module:376 sites/all/modules/custom/tiapp/modules/tiapp-push-notification/includes/tiapp_push_notification.admin.inc:69
msgid "Message"
msgstr ""

#: sites/all/modules/custom/tiapp/modules/tiapp-push-notification/tiapp_push_notification.module:377
msgid "Scheduled Push Date"
msgstr ""

#: sites/all/modules/custom/tiapp/modules/tiapp-push-notification/tiapp_push_notification.module:378 sites/all/modules/custom/tiapp/modules/tiapp-push-notification/includes/tiapp_push_notification.admin.inc:39
msgid "Status"
msgstr ""

#: sites/all/modules/custom/tiapp/modules/tiapp-push-notification/tiapp_push_notification.module:379 sites/all/modules/custom/tiapp/modules/tiapp-push-notification/includes/tiapp_push_notification.admin.inc:58
msgid "Title"
msgstr ""

#: sites/all/modules/custom/tiapp/modules/tiapp-push-notification/tiapp_push_notification.module:380
msgid "Operations"
msgstr ""

#: sites/all/modules/custom/tiapp/modules/tiapp-push-notification/tiapp_push_notification.module:416 sites/all/modules/custom/tiapp/modules/tiapp-push-notification/includes/tiapp_push_notification.theme.inc:48
msgid "Edit"
msgstr ""

#: sites/all/modules/custom/tiapp/modules/tiapp-push-notification/tiapp_push_notification.module:417 sites/all/modules/custom/tiapp/modules/tiapp-push-notification/includes/tiapp_push_notification.admin.inc:89
msgid "Delete"
msgstr ""

#: sites/all/modules/custom/tiapp/modules/tiapp-push-notification/tiapp_push_notification.module:423
msgid "Basic Search"
msgstr ""

#: sites/all/modules/custom/tiapp/modules/tiapp-push-notification/tiapp_push_notification.module:430
msgid "Search Term"
msgstr ""

#: sites/all/modules/custom/tiapp/modules/tiapp-push-notification/tiapp_push_notification.module:436
msgid "Search"
msgstr ""

#: sites/all/modules/custom/tiapp/modules/tiapp-push-notification/tiapp_push_notification.module:441
msgid "Bulk Operations"
msgstr ""

#: sites/all/modules/custom/tiapp/modules/tiapp-push-notification/tiapp_push_notification.module:449
msgid "Select a bulk operation"
msgstr ""

#: sites/all/modules/custom/tiapp/modules/tiapp-push-notification/tiapp_push_notification.module:450
msgid "Delete selected notifications"
msgstr ""

#: sites/all/modules/custom/tiapp/modules/tiapp-push-notification/tiapp_push_notification.module:456
msgid "Submit"
msgstr ""

#: sites/all/modules/custom/tiapp/modules/tiapp-push-notification/tiapp_push_notification.module:464
msgid "There are no notifications."
msgstr ""

#: sites/all/modules/custom/tiapp/modules/tiapp-push-notification/tiapp_push_notification.module:78
msgid "DUDE Push Notifications"
msgstr ""

#: sites/all/modules/custom/tiapp/modules/tiapp-push-notification/tiapp_push_notification.module:86
msgid "New Push Notification"
msgstr ""

#: sites/all/modules/custom/tiapp/modules/tiapp-push-notification/tiapp_push_notification.module:95
msgid "Manage Fields"
msgstr ""

#: sites/all/modules/custom/tiapp/modules/tiapp-push-notification/tiapp_push_notification.module:100
msgid "Manage Dependencies"
msgstr ""

#: sites/all/modules/custom/tiapp/modules/tiapp-push-notification/tiapp_push_notification.module:105
msgid "Manage Display"
msgstr ""

#: sites/all/modules/custom/tiapp/modules/tiapp-push-notification/tiapp_push_notification.module:110
msgid "Bulk Delete Notifications"
msgstr ""

#: sites/all/modules/custom/tiapp/modules/tiapp-push-notification/tiapp_push_notification.module:118
msgid "Push Notification Fields"
msgstr ""

#: sites/all/modules/custom/tiapp/modules/tiapp-push-notification/tiapp_push_notification.info:0
msgid "Tiapp Push Notifications"
msgstr ""

#: sites/all/modules/custom/tiapp/modules/tiapp-push-notification/tiapp_push_notification.info:0
msgid "This Tiapp Drupal module allows administrators to send push-notifications to the mobile app via Appecelerator's REST API. It supports direct messages to mobile users and data update notifications."
msgstr ""

#: sites/all/modules/custom/tiapp/modules/tiapp-push-notification/tiapp_push_notification.info:0
msgid "Tiapp"
msgstr ""

#: sites/all/modules/custom/tiapp/modules/tiapp-push-notification/includes/tiapp_push_notification.admin.inc:41
msgid "Submitted"
msgstr ""

#: sites/all/modules/custom/tiapp/modules/tiapp-push-notification/includes/tiapp_push_notification.admin.inc:41
msgid "Pending"
msgstr ""

#: sites/all/modules/custom/tiapp/modules/tiapp-push-notification/includes/tiapp_push_notification.admin.inc:42
msgid "The body text of the notification. A maximum of 255 characters are allowed."
msgstr ""

#: sites/all/modules/custom/tiapp/modules/tiapp-push-notification/includes/tiapp_push_notification.admin.inc:46
msgid "Scheduled push date"
msgstr ""

#: sites/all/modules/custom/tiapp/modules/tiapp-push-notification/includes/tiapp_push_notification.admin.inc:49
msgid "The time and date on which the notification is scheduled for submission."
msgstr ""

#: sites/all/modules/custom/tiapp/modules/tiapp-push-notification/includes/tiapp_push_notification.admin.inc:61
msgid "The title of the notification as it appears in the mobile device. This is also used in notifications list."
msgstr ""

#: sites/all/modules/custom/tiapp/modules/tiapp-push-notification/includes/tiapp_push_notification.admin.inc:72
msgid "The body text of the notification."
msgstr ""

#: sites/all/modules/custom/tiapp/modules/tiapp-push-notification/includes/tiapp_push_notification.admin.inc:86
msgid "Update Push Notification"
msgstr ""

#: sites/all/modules/custom/tiapp/modules/tiapp-push-notification/includes/tiapp_push_notification.admin.inc:86
msgid "Schedule Push Notification"
msgstr ""

#: sites/all/modules/custom/tiapp/modules/tiapp-push-notification/includes/tiapp_push_notification.admin.inc:119
msgid "@username notification has been saved."
msgstr ""

#: sites/all/modules/custom/tiapp/modules/tiapp-push-notification/includes/tiapp_push_notification.admin.inc:149
msgid "Delete all notifications?"
msgstr ""

#: sites/all/modules/custom/tiapp/modules/tiapp-push-notification/includes/tiapp_push_notification.admin.inc:149
msgid "Placeholder description"
msgstr ""

#: sites/all/modules/custom/tiapp/modules/tiapp-push-notification/includes/tiapp_push_notification.admin.inc:149
msgid "Delete all"
msgstr ""

#: sites/all/modules/custom/tiapp/modules/tiapp-push-notification/includes/tiapp_push_notification.admin.inc:149
msgid "Cancel"
msgstr ""

#: sites/all/modules/custom/tiapp/modules/tiapp-push-notification/includes/tiapp_push_notification.admin.inc:159
msgid "notifications deleted"
msgstr ""

#: sites/all/modules/custom/tiapp/modules/tiapp-push-notification/includes/tiapp_push_notification.theme.inc:29
msgid "website"
msgstr ""

#: sites/all/modules/custom/tiapp/modules/tiapp-push-notification/includes/tiapp_push_notification.theme.inc:33
msgid "contact form"
msgstr ""

#: sites/all/modules/custom/tiapp/modules/tiapp-push-notification/includes/tiapp_push_notification.theme.inc:39
msgid "twitter"
msgstr ""

#: sites/all/modules/custom/tiapp/modules/tiapp-push-notification/includes/tiapp_push_notification.theme.inc:43
msgid "youtube"
msgstr ""

